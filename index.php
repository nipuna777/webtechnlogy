<?php require 'config.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Utilizing SOAP Services  Nipuna Gunathilake</title>
    <link rel="stylesheet" href="style.css">

    <script src="js/jquery-1.11.1.min.js"/>

    <script language=JavaScript>
        function reload(form) {
            var val = form.cat.options[form.cat.options.selectedIndex].value;
            self.location = 'index.php?cat=' + val;
        }
    </script>

    <script type="text/javascript" language=JavaScript>
        function getWeather(city) {
            $("#spinner").show();
            var weatherAPI = "http://api.openweathermap.org/data/2.5/weather?";
            $.getJSON(weatherAPI, {
                q: city,
                units: "metric",
            })
                .done(function (data) {
                    var city = data.name;
                    var description = data.weather[0].description;
                    var mainInfo = data.weather[0].main;
                    var minTemp = data.main.temp_min;
                    var maxTemp = data.main.temp_max;
                    var humidity = data.main.humidity;
                    var image = data.weather[0].icon;
                    var imgUrl = "http://openweathermap.org/img/w/" + image + ".png";

                    $("#image").attr("src", imgUrl);
                    $("#desc").text(description);
                    $("#main").text(mainInfo);
                    $("#city").text(city);
                    $("#minTemp").text(minTemp);
                    $("#maxTemp").text(maxTemp);
                    $("#humi").text(humidity);
                    
                    $("#spinner").hide();
                    $("#imageButton").click();
                });
        }

        function myjsonpfunction(data) {
            //$("#imageButton").click();
            var url = data.responseData.results[0].url;
            $('html').css('background-image', 'url(' + url + ')');
            $("#spinner").hide();
        }


        function getImage(weather) {
            $("#spinner").show();
            //alert("test");
            var googleAPI = "https://ajax.googleapis.com/ajax/services/search/images?";

            $.ajax({
                url: 'https://ajax.googleapis.com/ajax/services/search/images?&v=1.0&q=' + document.getElementById('desc').innerHTML + '&imgtype=photo&callback=myjsonpfunction&as_sitesearch=flickr.com',
                type: "GET",
                dataType: 'jsonp',
                jsonp: 'myjsonpfunction',
                async: 'false',
                success: function (data) {
                }
            });

        }

        function reload(form) {
            var val = form.cat.options[form.cat.options.selectedIndex].value;
            self.location = 'index.php?cat=' + val;
        }

        function refreshWeather(form) {

            var val = $("#cityList option:selected").text();
            //self.location = 'index.php?cat=' + val;
            getWeather(val);
        }
    </script>

</head>
<body>
<?Php

@$cat = $_GET['cat'];

if (strlen($cat) > 0 and !is_numeric($cat)) {
    echo "Data Error";
    exit;
}

///////// Getting the data from Mysql table for first list box//////////
$quer2 = "SELECT DISTINCT category,cat_id FROM category order by category";

/////// for second drop down list we will check if category is selected else we will display all the subcategory/////
if (isset($cat) and strlen($cat) > 0) {
    $quer = "SELECT DISTINCT subcategory FROM subcategory where cat_id=$cat order by subcategory";
} else {
    $quer = "SELECT DISTINCT subcategory FROM subcategory order by subcategory";
}
echo "<div id=\"weather\">";
echo "<form method=post name=f1 action='dd-check.php'>";
/// Add your form processing page address to action in above line. Example  action=dd-check.php////


//////////        Starting of first drop downlist /////////
echo "<select name='cat' onchange=\"reload(this.form)\"><option value=''>Select one</option>";
foreach ($dbo->query($quer2) as $noticia2) {
    if ($noticia2['cat_id'] == @$cat) {
        echo "<option selected value='$noticia2[cat_id]'>$noticia2[category]</option>" . "<BR>";
    } else {
        echo "<option value='$noticia2[cat_id]'>$noticia2[category]</option>";
    }
}
echo "</select>";


//////////  Starting of second drop downlist /////////
echo "<select id='cityList' name='subcat' onchange=\"refreshWeather(this.form)\"><option value=''>Select one</option>";

foreach ($dbo->query($quer) as $noticia) {
    echo "<option value='$noticia[subcategory]'>$noticia[subcategory]</option>";
}
echo "</select>";
//////////////////  This will end the second drop down list ///////////
//// Add your other form fields as needed here/////
echo "</form>";
?>

<h1 id="city">City Name</h1>

<h2 style="margin-bottom: -20px;" id="main">Weather Info</h2>

<h3 id="moreDesc">(<span id="desc">more Info</span>)</h3>

<p style="margin-bottom: -20px;"><img id="image" src="http://openweathermap.org/img/w/02d.png" alt=""></p>

<p>Min Temprature: <span id="minTemp">--</span> C</p>

<p>Max Temprature: <span id="maxTemp">--</span> C</p>

<p>Humidity: <span id="humi">--</span>%</p>


<script type="text/javascript">
    //alert(document.getElementById('desc').innerHTML);
    var x = document.getElementById('desc').innerHTML;
    //alert($("#cityList option:selected").text();)
    //alert();
    //getWeather("kelaniya");


</script>

<button id="imageButton" onclick="getImage(x)">Refresh</button>

<img id="spinner" src="img/ajax-loader.gif" alt="">

<script type="text/javascript">
    $("#spinner").hide();
</script>
</div>


</body>
</html>